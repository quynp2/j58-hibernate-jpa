package com.devcamp.j58hibernate.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.devcamp.j58hibernate.model.CVoucher;

public interface IVoucherRepository extends JpaRepository<CVoucher, Long> {

}
