package com.devcamp.j58hibernate.controller;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.devcamp.j58hibernate.model.CVoucher;
import com.devcamp.j58hibernate.repository.IVoucherRepository;

@RestController
@CrossOrigin
@RequestMapping("/")
public class CVoucherController {
    @Autowired
    IVoucherRepository pVoucherRepository;

    @GetMapping("/vouchers")
    public ArrayList<CVoucher> getListVoucher() {
        ArrayList<CVoucher> lstVoucher = new ArrayList<>();
        pVoucherRepository.findAll().forEach(lstVoucher::add);

        return lstVoucher;

    }

    @GetMapping("/vouchers2")
    public ResponseEntity<List<CVoucher>> getAllVoucher() {
        try {
            ArrayList<CVoucher> listVoucher = new ArrayList<>();
            pVoucherRepository.findAll().forEach(listVoucher::add);
            if (listVoucher.size() == 0) {
                return new ResponseEntity<>(listVoucher, HttpStatus.NOT_FOUND);
            } else {
                return new ResponseEntity<>(listVoucher, HttpStatus.OK);
            }
        } catch (Exception e) {
            // TODO: handle exception
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

}
